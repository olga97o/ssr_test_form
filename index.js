const express = require('express'),
    AppComponent = express(),
    template = require('./views/template')
    path = require('path');


// Serving static files
AppComponent.use('/assets', express.static(path.resolve(__dirname, 'assets')));
AppComponent.use('/media', express.static(path.resolve(__dirname, 'media')));

// hide powered by express
AppComponent.disable('x-powered-by');
// start the server
AppComponent.listen(process.env.PORT || 3000);

// our apps data model

let initialState = {

    isFormSending: false,
    isSentNotification: false
};

//SSR function import
const ssr = require('./views/server');

// server rendered home page
AppComponent.get('/', (req, res) => {
    const {preloadedState, content} = ssr(initialState);
    const response = template("Server Rendered Page", preloadedState, content);
    res.setHeader('Cache-Control', 'assets, max-age=604800');
    res.send(response);
});

// Pure client side rendered page
AppComponent.get('/client', (req, res) => {
    let response = template('Client Side Rendered page');
    res.setHeader('Cache-Control', 'assets, max-age=604800');
    res.send(response);
});

// tiny trick to stop server during local development

AppComponent.get('/exit', (req, res) => {
    if (process.env.PORT) {
        res.send("Sorry, the server denies your request")
    } else {
        res.send("shutting down");
        process.exit(0);
    }

});