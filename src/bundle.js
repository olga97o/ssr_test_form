import React from 'react';
import {render} from 'react-dom';
import {Provider} from "react-redux";
import axios from 'axios';

import AppComponent from "./components/AppComponent";
import StoreComponent from "./components/StoreComponent";

//import './assets/AppComponent.css';

axios.defaults.baseURL = ' http://test.clevertec.ru/tt';

const store = StoreComponent();

    render(
        <Provider store={store}>
            <AppComponent/>
        </Provider>, document.getElementById('root')
    );