// html skeleton provider
function template(title, initialState = {}, content = ""){
    let scripts = ''; // Dynamically ship scripts based on render type
    if(content){
        scripts = ` <script>
                   window.__STATE__ = ${JSON.stringify(initialState)}
                </script>
                <script src="client.js"></script>
                `
    } else {
        scripts = ` <script src="bundle.js"> </script> `
    }
    let page = `<!DOCTYPE html>
              <html lang="en">
              <head>
                <meta charset="utf-8">
                <title>${title}</title>
              </head>
              <body>
                <form class="content">
                   <div>
                      <!--- magic happens here -->  ${content}
                   </div>
                </form>
                  ${scripts}
              </body>
              `;

    return page;
}

module.exports = template;