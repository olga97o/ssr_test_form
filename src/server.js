import React from 'react';
import {renderToString} from 'react-dom/server';
import {Provider} from 'react-redux';

import StoreComponent from './components/StoreComponent';
import AppComponent from './components/AppComponent';

module.exports = function render(initialState) {
    // Model the initial state
    const store = StoreComponent(initialState);

    let content = renderToString(
        <Provider store={store}>
            <AppComponent/>
        </Provider>);
    const preloadedState = store.getState();
    return {
        content,
        preloadedState
    };
};