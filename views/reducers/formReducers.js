"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = _default;

var _formTypes = require("../actions/formTypes");

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var initialState = {
  form: null,
  isFormSending: false,
  isSentNotification: false
};

function _default() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : initialState;
  var action = arguments.length > 1 ? arguments[1] : undefined;
  var type = action.type,
      payload = action.payload;

  switch (type) {
    case _formTypes.GET_FORM_DATA:
      return _objectSpread({}, state, {
        form: payload
      });

    case _formTypes.SET_INPUT_VALUE:
      return _objectSpread({}, state, {
        form: _objectSpread({}, state.form, _defineProperty({}, payload.name, payload.value))
      });

    case _formTypes.SET_SENDING_STATUS:
      return _objectSpread({}, state, {
        isFormSending: payload
      });

    case _formTypes.SET_SEND_NOTIFICATION_STATUS:
      return _objectSpread({}, state, {
        isSentNotification: payload
      });

    default:
      return state;
  }
}