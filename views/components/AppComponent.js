"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireWildcard(require("react"));

var _reactRedux = require("react-redux");

var _formActions = require("../actions/formActions");

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/*
import Input from 'arui-feather/input';
import Button from 'arui-feather/button';
import Form from "arui-feather/form";
import Select from "arui-feather/select";
import FormField from "arui-feather/form-field";
import Spin from "arui-feather/spin";
import Notification from "arui-feather/notification";
*/
//import '../assets/AppComponent.css';
var AppComponent =
/*#__PURE__*/
function (_Component) {
  _inherits(AppComponent, _Component);

  function AppComponent() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, AppComponent);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(AppComponent)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_this), "state", {
      finalForm: {}
    });

    _defineProperty(_assertThisInitialized(_this), "handleChange", function (e, name) {
      _this.setState({
        finalForm: _objectSpread({}, _this.state.finalForm, _defineProperty({}, name, e))
      }); //console.log(name);
      //setInputValue({value: e, name});

    });

    _defineProperty(_assertThisInitialized(_this), "onSubmit", function (e) {
      //console.log('hello, my friend');
      _this.props.sendFormData(_this.state.finalForm);
    });

    _defineProperty(_assertThisInitialized(_this), "setInputType", function (type, values, name) {
      switch (type) {
        case 'TEXT':
          return _react["default"].createElement("input", {
            type: "text",
            name: name,
            onChange: function onChange(e) {
              return _this.handleChange(e, name);
            }
          });

        case 'NUMERIC':
          return _react["default"].createElement("input", {
            type: "number",
            name: name,
            onChange: function onChange(e) {
              return _this.handleChange(e, name);
            }
          });

        /*case 'LIST':
            const options = [
                {value: values.none, text: values.none},
                {value: values.v1, text: values.v1},
                {value: values.v2, text: values.v2},
                {value: values.v3, text: values.v3}
            ];
            return <Select name={name} mode="radio-check" options={options}
                           onChange={e => this.handleChange(e[0], name)}/>*/
      }
    });

    return _this;
  }

  _createClass(AppComponent, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      this.props.getFormData();
    }
  }, {
    key: "render",
    value: function render() {
      var _this2 = this;

      var _this$props = this.props,
          form = _this$props.form,
          isFormSending = _this$props.isFormSending,
          isSentNotification = _this$props.isSentNotification,
          setSendNotificationStatus = _this$props.setSendNotificationStatus;
      console.log(this.props);
      return form ? _react["default"].createElement("form", {
        onSubmit: this.onSubmit,
        className: "App"
      }, _react["default"].createElement("h2", null, form.title), _react["default"].createElement("img", {
        src: form.image,
        alt: "image"
      }), _react["default"].createElement("table", {
        border: "1"
      }, _react["default"].createElement("tbody", null, form.fields.map(function (field, index) {
        return _react["default"].createElement("tr", {
          key: index
        }, _react["default"].createElement("td", null, _react["default"].createElement("h5", null, field.title)), _react["default"].createElement("td", null, _this2.setInputType(field.type, field.values, field.name)));
      }))), _react["default"].createElement("button", {
        view: "extra",
        type: "submit",
        icon: _react["default"].createElement("spin", {
          visible: isFormSending
        })
      }, "Send")) : _react["default"].createElement("div", null, "Loading");
    }
  }]);

  return AppComponent;
}(_react.Component);

var mapStateToProps = function mapStateToProps(state) {
  return {
    //console.log(state)
    isFormSending: state.isFormSending,
    isSentNotification: state.isSentNotification,
    form: state.form
  };
};

var _default = (0, _reactRedux.connect)(mapStateToProps, {
  getFormData: _formActions.getFormData,
  sendFormData: _formActions.sendFormData,
  setInputValue: _formActions.setInputValue,
  setSendNotificationStatus: _formActions.setSendNotificationStatus
})(AppComponent);

exports["default"] = _default;