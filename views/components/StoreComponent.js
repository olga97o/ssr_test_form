"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = StoreComponent;

var _redux = require("redux");

var _reduxThunk = _interopRequireDefault(require("redux-thunk"));

var _formReducers = _interopRequireDefault(require("../reducers/formReducers"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function StoreComponent(preloadedState) {
  return (0, _redux.createStore)(_formReducers["default"], preloadedState, (0, _redux.applyMiddleware)(_reduxThunk["default"]));
}