"use strict";

var _react = _interopRequireDefault(require("react"));

var _server = require("react-dom/server");

var _reactRedux = require("react-redux");

var _StoreComponent = _interopRequireDefault(require("./components/StoreComponent"));

var _AppComponent = _interopRequireDefault(require("./components/AppComponent"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

module.exports = function render(initialState) {
  // Model the initial state
  var store = (0, _StoreComponent["default"])(initialState);
  var content = (0, _server.renderToString)(_react["default"].createElement(_reactRedux.Provider, {
    store: store
  }, _react["default"].createElement(_AppComponent["default"], null)));
  var preloadedState = store.getState();
  return {
    content: content,
    preloadedState: preloadedState
  };
};