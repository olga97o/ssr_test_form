"use strict";

var _react = _interopRequireDefault(require("react"));

var _reactDom = require("react-dom");

var _reactRedux = require("react-redux");

var _axios = _interopRequireDefault(require("axios"));

var _AppComponent = _interopRequireDefault(require("./components/AppComponent"));

var _StoreComponent = _interopRequireDefault(require("./components/StoreComponent"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

//import './assets/AppComponent.css';
_axios["default"].defaults.baseURL = ' http://test.clevertec.ru/tt';
var store = (0, _StoreComponent["default"])();
(0, _reactDom.render)(_react["default"].createElement(_reactRedux.Provider, {
  store: store
}, _react["default"].createElement(_AppComponent["default"], null)), document.getElementById('root'));