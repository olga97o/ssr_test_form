"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.setSendNotificationStatus = exports.sendFormData = exports.setInputValue = exports.getFormData = void 0;

var _axios = _interopRequireDefault(require("axios"));

var _formTypes = require("./formTypes");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var getFormData = function getFormData() {
  return function _callee(dispatch) {
    var res;
    return regeneratorRuntime.async(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            try {
              // await axios.post('/meta');
              console.log('hello');
              res = {
                title: 'My Test Form',
                image: 'https://resheto.net/images/mater/GoodMorning/kartinki_s_dobrym_utrom_24.jpeg',
                fields: [{
                  title: 'text',
                  name: 'text',
                  type: 'TEXT',
                  values: ''
                }, {
                  title: 'numeric',
                  name: 'numeric',
                  type: 'NUMERIC',
                  values: ''
                }, {
                  title: 'list',
                  name: 'list',
                  type: 'LIST',
                  values: {
                    none: "Не выбрано",
                    v1: "Первое значение",
                    v2: "Второе значение",
                    v3: "Третье значение"
                  }
                }]
              };
              dispatch({
                type: _formTypes.GET_FORM_DATA,
                payload: res
              });
            } catch (error) {
              console.log(error);
            }

          case 1:
          case "end":
            return _context.stop();
        }
      }
    });
  };
};

exports.getFormData = getFormData;

var setInputValue = function setInputValue(input) {
  return function (dispatch) {
    console.log('input', input);
    dispatch({
      type: _formTypes.SET_INPUT_VALUE,
      payload: input
    });
  };
};

exports.setInputValue = setInputValue;

var sendFormData = function sendFormData(e) {
  return function _callee2(dispatch) {
    return regeneratorRuntime.async(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            try {
              dispatch({
                type: _formTypes.SET_SENDING_STATUS,
                payload: true
              }); // const res = await axios.post('/data', null, e);

              /* setTimeout(()=> {dispatch({
                       type: SET_SENDING_STATUS,
                       payload: false
                   })}, 1000);
               setTimeout(()=> { dispatch({
                   type: SET_SEND_NOTIFICATION_STATUS,
                   payload: true
               })}, 1010);
              ;*/

              /*dispatch({
                  type: SEND_FORM_DATA
              });*/

              console.log('target', e);
            } catch (error) {
              console.log('error', error);
            }

          case 1:
          case "end":
            return _context2.stop();
        }
      }
    });
  };
};

exports.sendFormData = sendFormData;

var setSendNotificationStatus = function setSendNotificationStatus(status) {
  return function (dispatch) {
    dispatch({
      type: _formTypes.SET_SEND_NOTIFICATION_STATUS,
      payload: status
    });
  };
};

exports.setSendNotificationStatus = setSendNotificationStatus;